package com.rtmp.sample;

import org.junit.Test;
import com.rtmp.librtmp.*;

import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {

    //此处需要替换为你自己的RTMP服务器地址
    private static final String HOST = "";

    @Test
    public void rtmpAlloc() {
        boolean result = RtmpUtil.allocRtmp();
        assertTrue(result);
    }

    @Test
    public void initRtmp() {
        boolean result = RtmpUtil.initRtmp();
        assertTrue(result);
    }

    @Test
    public void setupUrl() {
        boolean result = RtmpUtil.setupUrl(HOST);
        assertTrue(result);
    }

    @Test
    public void connect() {
        boolean result = RtmpUtil.connect(HOST);
        assertTrue(result);
    }

    @Test
    public void connectStream() {
        boolean result = RtmpUtil.connectStream(HOST);
        assertTrue(result);
    }

    @Test
    public void enableWrite() {
        boolean result = RtmpUtil.enableWrite(HOST);
        assertTrue(result);
    }

    @Test
    public void pause() {
        boolean result = RtmpUtil.pause(HOST, true);
        assertTrue(result);
    }

    @Test
    public void play() {
        boolean result = RtmpUtil.pause(HOST, false);
        assertTrue(result);
    }

    @Test
    public void seek() {
        boolean result = RtmpUtil.seek(HOST);
        assertTrue(result);
    }

    @Test
    public void free() {
        boolean result = RtmpUtil.freeTest();
        assertTrue(result);
    }
}