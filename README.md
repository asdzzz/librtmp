## librtmp

本项目是基于开源项目librtmp进行openharmony的移植和开发的，可以通过项目标签以及github地址([https://github.com/yixia/librtmp](https://github.com/yixia/librtmp))追踪到原安卓项目版本。

基线版本：https://github.com/yixia/librtmp commit:b0631b09b5baa3a62df9685e0e9e58d51d7726f4

### 项目介绍

* 项目名称：librtmp（Librtmp是用于RTMP流的工具包。 支持所有形式的RTMP，包括rtmp://，rtmpt://，rtmpe://，rtmpte://和rtmps://）
* 所属系列：openharmony的第三方组件适配移植
* 功能：Librtmp是用于RTMP流的工具包。 支持所有形式的RTMP，包括rtmp://，rtmpt://，rtmpe://，rtmpte://和rtmps://。
* 项目移植状态：完成
* 调用差异：无
* 开发版本：**SDK 5 Release版本**（Demo版本，库可直接在C/C++调用），DevEco Studio 2.1 Beta 4）
* 项目作者和维护人：冯仁杰
* 联系方式：fengrenjie@uniontech.com
* 原项目Doc地址：https://github.com/yixia/librtmp

### 项目介绍

* 编程语言：C++
* 外部库依赖：openssl(本地已内置)

### 安装教程

#### OpenHarmony HAP中使用

1. 将`librtmp/src/main/cpp/librtmp`和`librtmp/src/main/cpp/ssl`文件夹复制到你的cpp目录

2. 修改你的cmakelist.txt文件，添加如下代码：

   ```shell
   #引入指定目录下的CMakeLists.txt
   add_subdirectory(librtmp)
   #指定头文件查找路径
   include_directories(librtmp)
   
   #rtmp为librtmp库名称，hilog_ndk.z为hilog 日志引用
   target_link_libraries(${PROJECT_NAME} rtmp hilog_ndk.z)
   ```

   

3. 在entry的build.gradle中添加如下代码：

   ```groovy
   ohos {
     
       ...
   
       externalNativeBuild {
           path "src/main/cpp/CMakeLists.txt"
           arguments "-v"
           abiFilters "arm64-v8a"
           cppFlags "-std=c++11"
       }
   }
   ```

4. 在你的.c/.cpp文件中，使用librtmp

   ```c++
   #include <rtmp.h>
   
   //分配一个RTMP
   RTMP *rtmp = RTMP_Alloc();
   //初始化
   RTMP_Init(rtmp);
   
   //定义rtmp服务器地址
   const char *url;
   
   //设置RTMP服务器
   int result = RTMP_SetupURL(rtmp, (char *) url);
   if(result){
       
       //推流需要启用
       //开启写入，在connect之前调用
       //RTMP_EnableWrite(rtmp);
       
       //直播拉流需要启用
       //表示直播流
       //rtmp->Link.lFlags |= RTMP_LF_LIVE;
       
       //连接服务器
       result = RTMP_Connect(rtmp, NULL);
       if(result){
           //连接流
           result = RTMP_ConnectStream(rtmp, 0);
           if(result){
               //推流和拉流分别实现，请参考sample
               ...
               
               //关闭rtmp流    
               RTMP_Close(rtmp);
           }
       }
       //清理会话
       RTMP_Free(rtmp);
       rtmp=nullptr;
   }
   ```

5. 具体示例（`cmakelist.txt`范例和`sample.cpp`范例）可以在`librtmp/src/main/cpp/`中查看。

6. **注意：由于笔者的开发环境 (IDE版本DevEco Studio 2.1 beta4，插件版本：`com.huawei.ohos:hap:2.4.2.7`，SDK为5的release版本2.1.1.20)暂时不能将cmakelist.txt引用的动态库按原文件打包，故需要手动将依赖的动态库文件复制到libs目录。**以当前项目为例：`librtmp/src/main/cpp/CMakeLists.txt` 编译所生成的文件名`libsample.so`，`librtmp/src/main/cpp/librtmp/CMakeLists.txt`编译所生成的文件名`libc++.so`，`libc++.so`实际上是librtmp模块编译生成，其中打包了`libcrypto.so和libssl.so`，但是直接使用会出现找不到`libcrypto.so和libssl.so`的错误，故此，需要将`libcrypto.so和libssl.so`复制到libs对应的abi目录。



### 使用说明

```c
#include <rtmp.h>

//分配一个RTMP
RTMP *rtmp = RTMP_Alloc();
//初始化
RTMP_Init(rtmp);

//定义rtmp服务器地址
const char *url;

//设置RTMP服务器
int result = RTMP_SetupURL(rtmp, (char *) url);
if(result){
    
    //推流需要启用
    //开启写入，在connect之前调用
    //RTMP_EnableWrite(rtmp);
    
    //连接服务器
    result = RTMP_Connect(rtmp, NULL);
    if(result){
        //连接流
        result = RTMP_ConnectStream(rtmp, 0);
        if(result){
            //推流和拉流分别实现，请参考sample
            ...
            
            //关闭rtmp流    
            RTMP_Close(rtmp);
        }
    }
    //清理会话
    RTMP_Free(rtmp);
    rtmp=nullptr;
}
```



### 版本迭代

* v1.0.0

### 版权和许可信息

* [GPL-2.0 License](https://gitee.com/archermind-ti/librtmp_ohos/blob/master/LICENSE)

