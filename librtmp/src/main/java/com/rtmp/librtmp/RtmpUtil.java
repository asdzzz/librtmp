/**
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.rtmp.librtmp;

public class RtmpUtil {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("sample");
    }

    /**
     * 获取RTMP版本
     */
    public static native String getRtmpVersion();

    /**
     * 推流
     *
     * @param host rtmp流地址
     * @param path 文件地址
     */
    public static native boolean pushRtmp(String host, String path);

    /**
     * 拉流
     *
     * @param host rtmp流地址
     * @param path 保存文件地址
     */
    public static native boolean receiveRtmp(String host, String path);

    /**
     * 检查地址有效性
     *
     * @param host 服务器地址
     * @return true 表示地址有效，false表示地址无效
     */
    public static native boolean checkAddressValidity(String host);

    //=============从这开始，都为单元测试相关======================

    public static native boolean allocRtmp();

    public static native boolean initRtmp();

    public static native boolean setupUrl(String host);

    public static native boolean connect(String host);

    public static native boolean connectStream(String host);

    public static native boolean read(String host);

    public static native boolean enableWrite(String host);

    public static native boolean pause(String host, boolean isPause);

    public static native boolean seek(String host);

    public static native boolean freeTest();
}
